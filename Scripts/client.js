var socket;
var game = undefined;
var playerNum = undefined;
var activeGame = undefined;

$(document).ready(function() {
	socket = io();
	
	//Receive user id from server
	socket.on('assignUserId', function(id) {
		$('#usernameText').val("User " + id + ":");
	});
	
	//Receive player # (1-4) from server
	socket.on('assignPlayerNum', function(num) {
		playerNum = num;
	});
	
	//Receive chat message broadcast from server
	socket.on('chatmsg', function(msg) {
		var $chatMessages = $('#chatMessages');
		$chatMessages.val($('#chatMessages').val() + '\n' + msg);
		$chatMessages.scrollTop($chatMessages[0].scrollHeight);
	});
	
	//Receive pixel info from server
	socket.on('drawPixel', function(x, y, color) {
		drawPixelLocal(x, y, color);
	});
	
	//Receive clear pixel index from server
	socket.on('clearPixel', function(pixIndex) {
		clearPixelLocal(pixIndex);
	});
	
	//Handle signal to create new game instance
	socket.on('startGameSignal', function() {
		activeGame = new SnakeGame(socket, playerNum);
	});
	
	//Handle signal to cancel game 
	socket.on('cancelGameSignal', function() {
		game = undefined;
	});
	
	//End game and declare winner
	socket.on('gameWin', function(num) {
		activeGame.haltSnake();
		var $pInfoText = $('#playerIdentifierText');
		$pInfoText.text("Winner: player " + num + "!");
	});
	
	//Handle server message notifying player of new pellet in game area
	socket.on('createPellet', function(x, y) {
		createPellet(x, y);
	});
	
	//Handle server message notifying player of pellet removal from game area
	socket.on('deletePellet', function(index) {
		removePellet(index);
	});
	
	//Handle chat message submit
	$('#chatForm').submit(function() {
		socket.emit('chatmsg', $('#chatInput').val());
		$('#chatInput').val('');
		return false;
	});
});