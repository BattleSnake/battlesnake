To test locally:

- Make sure Node.js and npm are installed
- In terminal (git bash or cygwin on windows):
	- navigate to Scripts directory
	- run command: node server.js
- In browser:
	- navigate to localhost:3000/

Chat is ready to go, take a look at server.js and client.js for examples on socket.io and jQuery implementation.

TODO:
- implement game controls, loss conditions on client side
- handle win/loss on client side
- extend snake on pellet pickup
- expand game - powerups, timers, etc. 
- 

Please look at the new Implementations I've made. I have fixed the canvas size to a fixed size, and have pellets spawning all over.
The pellets should be spawning with different colors based on their values, but I'm getting some bugs.
In doing this I also have screwed up snake growth.
