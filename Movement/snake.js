var up = false;
var down = true;
var left = false;
var right = false;
var snakeX = 20;
var snakeY = 20;
var maxSnakeLength = 10; //Increase this to grow snake
var canvas = "";
var interval = undefined;
var gamePiece = undefined;
var gsocket = undefined;
var scolor = undefined;

const DEFAULT_PELLET_VAL = 5;
var activePellets = [];

var snakePixels = [];

class SnakeGame {

    constructor(socket, num) {

        this.PlayerInfo = {
            playerSocket: socket,
            playerNum: num,
            snakeColor: undefined,
            gamePiece: undefined,
            startX: undefined,
            startY: undefined,
            timer: undefined,
        };

        this.snakeColor = this.assignSnakeColor(playerNum);
        scolor = this.snakeColor;
        this.setPlayerInfo(socket, num);
        this.startSnake();

        return this;
    }

    //Assign a snake color based on player number
    assignSnakeColor(playerNum) {
        console.log("assigning color to " + playerNum);
        switch (playerNum) {
            case (1):
                return new RGBAColor(255, 0, 0, 255); //red
                break;
            case (2):
                return new RGBAColor(0, 0, 255, 255); //blue
                break;
            case (3):
                return new RGBAColor(0, 255, 0, 255); //green
                break;
            case (4):
                return new RGBAColor(255, 0, 255, 255); //purple
                break;
        }
    }

    //Set player info vars, display player info text under game window
    setPlayerInfo(socket, num) {
        //Assign instance vars
        this.playerSocket = socket;
        gsocket = this.playerSocket;
        this.playerNum = num;
        this.canvas = document.getElementById("gameCanvas");

        //Show player # and color
        var $pInfoText = $('#playerIdentifierText')
        $pInfoText.css('visibility', 'visible');

        var colorAsString = "";
        switch (num) {
            case (1):
                colorAsString = "red";
                this.startX = 0;
                this.startY = this.canvas.height / 2;
                setDirection("left");
                break;
            case (2):
                colorAsString = "blue";
                this.startX = this.canvas.width / 2;
                this.startY = this.canvas.height;
                setDirection("up");
                break;
            case (3):
                colorAsString = "green";
                this.startX = this.canvas.width;
                this.startY = this.canvas.height / 2;
                setDirection("right");
                break;
            case (4):
                colorAsString = "purple";
                this.startX = this.canvas.width / 2;
                this.startY = 0;
                setDirection("down");
                break;
        }

        $pInfoText.css('color', colorAsString);
        $pInfoText.text("You are player #" + parseInt(this.playerNum));
    }

    startSnake() {
        snakeX = this.startX;
        snakeY = this.startY;
        this.gamePiece = new component(5, 5, this.snakeColor, this.startX, this.startY, this.playerSocket);
        gamePiece = this.gamePiece;
        window.addEventListener("keydown", this.keyEventHandler, true);
        this.timer = setInterval(this.updateGameArea, 50);

    }

    keyEventHandler(event) {
        if (event.keyCode == 38) {
            setDirection("up");
        } else if (event.keyCode === 40) { /* Down arrow was pressed */
            setDirection("down");
        } else if (event.keyCode === 37) { /* right arrow was pressed */
            setDirection("right");
        } else if (event.keyCode === 39) { /* left arrow was pressed */
            setDirection("left");
        }
    }

    updateGameArea() {
        //Check if pixel is occupied
        canvas = document.getElementById("gameCanvas");
        var imgdata = canvas.getContext("2d").getImageData(0, 0, canvas.width, canvas.height);
        var pixIndex = (snakeY * canvas.width + snakeX) * 4;

        //Check if pellet
        if (isPellet(imgdata, pixIndex)) {
            //find pellet object in activePellets
            for (var i = 0; i < activePellets.length; i++) {
                if (activePellets[i].x == snakeX && activePellets[i].y == snakeY) {
                    this.playerSocket.emit('pelletFound', i);
                }
            }
        }
        /*
		if ((snakeX == 680)||(snakeX == 0)) {
			//window.alert("GAME OVER");
            //myGameArea.stop();
        }
		if ((snakeY == 550)||(snakeY == 0)) {
            //window.alert("GAME OVER");
            myGameArea.stop();
        }
		*/
        if (left == true) {
            this.gamePiece.x += 1;
            snakeX += 1;
        }
        if (down == true) {
            this.gamePiece.y += 1;
            snakeY += 1;
        }
        if (up == true) {
            this.gamePiece.y -= 1;
            snakeY -= 1;
        }
        if (right == true) {
            this.gamePiece.x -= 1;
            snakeX -= 1;
        }

        //gsocket.emit('drawPixel', snakeX, snakeY, scolor);    
        this.gamePiece.update();
    }
}

//Represents a color for drawing pixels
class RGBAColor {

    constructor(r, g, b, a) {

        this.colorInfo = {
            red: r,
            green: g,
            blue: b,
            alpha: a
        };

        return this;
    }

}

//Draw a pixel on the canvas
//Param color should be an instance of RGBAColor
//NOTE - should not be called directly by game - emit drawPixel event from socket instead
//		Example draw pixel event - socket.emit("drawPixel", x, y, color)
function drawPixelLocal(x, y, color) {
    //Context
    canvas = document.getElementById("gameCanvas");
    context = canvas.getContext("2d");
    imgdata = context.getImageData(0, 0, canvas.width, canvas.height);

    //Color specified pixel
    var pixIndex = (y * canvas.width + x) * 4;
    imgdata.data[pixIndex] = color.colorInfo.red;
    imgdata.data[pixIndex + 1] = color.colorInfo.green;
    imgdata.data[pixIndex + 2] = color.colorInfo.blue;
    imgdata.data[pixIndex + 3] = color.colorInfo.alpha;

    context.putImageData(imgdata, 0, 0);
}

//Erase any color present in pixel at pixIndex
function clearPixelLocal(pixIndex) {
    //Context
    canvas = document.getElementById("gameCanvas");
    context = canvas.getContext("2d");
    imgdata = context.getImageData(0, 0, canvas.width, canvas.height);

    imgdata.data[pixIndex] = 255;
    imgdata.data[pixIndex + 1] = 255;
    imgdata.data[pixIndex + 2] = 255;
    imgdata.data[pixIndex + 3] = 255;

    context.putImageData(imgdata, 0, 0);
}

//Clear the whole canvas
function clearCanvas() {
    canvas = document.getElementById("gameCanvas");
    context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.width, canvas.height);
}

//Create a new pellet in the game area
function createPellet(x, y) {
    //Context
    canvas = document.getElementById("gameCanvas");
    context = canvas.getContext("2d");
    imgdata = context.getImageData(0, 0, canvas.width, canvas.height);

    var pixIndex = (y * canvas.width + x) * 4;
    imgdata.data[pixIndex] = 0;
    imgdata.data[pixIndex + 1] = 0;
    imgdata.data[pixIndex + 2] = 0;
    imgdata.data[pixIndex + 3] = 255;

    context.putImageData(imgdata, 0, 0);

    newPellet = new Pellet(x, y);
    activePellets.push(newPellet);
}

function removePellet(index) {
    canvas = document.getElementById("gameCanvas");
    var pixIndex = (activePellets[i].y * canvas.width + activePellets[i].x) * 4;
    clearPixelLocal(pixIndex);
    activePellets.splice(index);
}

//Check if the given pixel contains a pellet
//Return true if yes, false if no
function isPellet(imgdata, pixIndex) {
    if (imgdata.data[pixIndex] == 0 &&
        imgdata.data[pixIndex + 1] == 0 &&
        imgdata.data[pixIndex + 2] == 0 &&
        imgdata.data[pixIndex + 3] == 255) {
        return true;
    }
    return false;
}

function setDirection(dir) {
    switch (dir) {
        case "up":
            if (down == true) {
                break;
            }
            up = true;
            right = false;
            left = false;
            down = false;
            break;
        case "down":
            if (up == true) {
                break;
            }
            up = false;
            right = false;
            left = false;
            down = true;
            break;
        case "left":
            if(right == true)
                {
                    break;
                }
            up = false;
            right = false;
            left = true;
            down = false;
            break;
        case "right":
            if(left == true)
                {
                    break;
                }
            up = false;
            right = true;
            left = false;
            down = false;
            break;
    }
}

class component {

    constructor(w, h, col, xloc, yloc, socket) {
        this.width = w;
        this.height = h;
        this.x = xloc;
        this.y = yloc;
        this.gsocket = socket;
    }

    update() {
        this.gsocket.emit('drawPixel', snakeX, snakeY, scolor);
        snakePixels.push((snakeY * document.getElementById("gameCanvas").width + snakeX) * 4); //Push pixel index of next snake position
        if (snakePixels.length > maxSnakeLength) {
            var pixToClear = snakePixels.shift();
            this.gsocket.emit('clearPixel', pixToClear);
        }
    }
}

class Pellet {

    constructor(x, y, value = DEFAULT_PELLET_VAL) {
        this.x = x;
        this.y = y;
        this.value = value;
    }

}
