var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var globalUserCount = 0;
var anon_user_ids = [];
var active_players = [];
var losers = [];
var gameActive = false;

var pelletTimer = undefined;

const PLAYERCOUNT = 2;

//Point to relative directories
app.use('/Styles', express.static(__dirname + '/../Styles'));
app.use('/Scripts', express.static(__dirname + '/../Scripts'));

app.get('/', function(req, res){
	//Serve index.html to client
	res.sendFile('index.html', {root: "../Views/"});
});

//Behavior on user connect goes here
io.on('connection', function(socket) {
	console.log("accepted socket connection");
	var user_id = anon_user_ids.push(globalUserCount++); //Assign id to user (change this in future to username?)
	console.log("user id assigned: " + user_id);
	
	//Assign numerical user id
	socket.emit('assignUserId', user_id);
	
	//Assign player #, if empty player slots
	if (active_players.length < PLAYERCOUNT) {
		active_players.push(socket);
		socket.emit('assignPlayerNum', active_players.length);
		//If all players present, send signal to start the game
		if (active_players.length == PLAYERCOUNT) {
			console.log("4 players present, starting the game");
			io.emit('startGameSignal');
			pelletTimer = setInterval(createPellet, 5000);
			gameActive = true;
		}
	}
	
	//Handle chat message event - broadcast to all clients
	socket.on('chatmsg', function(msg) {
		io.emit('chatmsg', "User " + user_id + ": " + msg);
		console.log("message from " + user_id + ": " + msg);
	});
	
	//Handle draw pixel event - broadcast to all clients
	socket.on('drawPixel', function(x, y, color) {
		io.emit('drawPixel', x, y, color);
	});
	
	//Handle pixel clear event - broadcast to all clients
	socket.on('clearPixel', function(pixIndex) {
		io.emit('clearPixel', pixIndex);
	});
	
	//Handle player consuming pellet - tell all clients to remove
	socket.on('pelletFound', function(pelletIndex) {
		io.emit('deletePellet', pelletIndex);
	});
	
	//Behavior on user disconnect goes here
	socket.on('disconnect', function(){
		console.log("user " + user_id + " has disconnected");
		
		//Check if disconnected user was active player - if so, cancel game
		var i = 0;
		for (i = 0; i < active_players.length; i++) {
			if (active_players[i] == socket) {
				io.emit('cancelGameSignal');
				console.log("Player " + (i+1) + " has left, cancelling game");
				gameActive = false;
				active_players = [];
			}
		}
	});

	//Handle game loss
	socket.on('gameLoss', function(playerNum) {
		losers.push(playerNum);
		if (losers.length == 3) {
			//game over, assign winner
			var i = 1;
			for (i = 1; i < 5; i++) {
				var isWinner = true;
				var j = 0;
				for (j = 0; j < 4; j++) {
					if (losers[j] == i) isWinner = false;
				}
				if (isWinner) {
					io.emit('gameWin', i);
					console.log("Winner: " + "Player " + i);
					return;
				}
			}
		}
	});
	
});

http.listen(3000, function(){
	console.log('listening on port 3000');
});

//Create a pellet in a random location and notify players
function createPellet() {
	//generate random x y coord
	x = Math.floor(Math.random() * 301);
	y = Math.floor(Math.random() * 151);
    size = Math.floor(Math.random() * (20 - 5 + 1)) + 5;
	console.log("new pellet at " + x + ", " + y, "with size: " + size);
	io.emit('createPellet', x, y, size);
}